from flask import Flask, request, render_template, send_file, redirect, url_for
import os
import subprocess
import time

app = Flask(__name__)
DOWNLOAD_FOLDER = '/media/videos/'

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/download', methods=['POST'])
def download():
    url = request.form['url']
    if not url:
        return "No URL provided", 400

    # Generate a unique filename using the current time
    filename = f"video-{int(time.time())}.mp4"
    filepath = os.path.join(DOWNLOAD_FOLDER, filename)

    # Run the curl command to download the video
    subprocess.run(['curl', '-o', filepath, url])

    return send_file(filepath, as_attachment=True)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
